import firebase from 'firebase/app';
import "firebase/auth";
import "firebase/database";
import "firebase/storage";
  // Initialize Firebase
var config = {
  apiKey: "AIzaSyDfGMPf6e-yM2jV5n_oGpxuX03R3SJAQO8",
  authDomain: "react-app-clone.firebaseapp.com",
  databaseURL: "https://react-app-clone.firebaseio.com",
  projectId: "react-app-clone",
  storageBucket: "react-app-clone.appspot.com",
  messagingSenderId: "602322610531"
};
firebase.initializeApp(config);

export default firebase;